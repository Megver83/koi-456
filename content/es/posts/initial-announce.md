+++
title = "Based Games, el nuevo paradigma en entretenimiento"
date = "2022-03-15"
author = "César Muñoz"
cover = "img/Based-Games.jpg"
+++

Todo comenzó un día de marzo del 2022, en la Universidad Católica del Norte de Coquimbo, Chile. Era el inicio de clases y la vuelta a la presencialidad. Fue entonces que a [seis destacados alumnos](/koi-456/es/about/#nuestro-equipo) de la carrera de ingeniería civil en computación e informática se les puso a prueba con el más grande y vertiginoso desafío: desarrollar el mejor videojuego jamás antes imaginado. Desafío que, con valentía, los atractivos estudiantes no dudaron en aceptar.

Fue así como nació **Based Games**, un equipo de entusiastas compuesto por talentosos artistas y programadores con el fin de crear algo más que un videojuego impactante: generar una experiencia inolvidable. No una moda, ni una tendencia. Soñamos con hacer algo que supere los límites del tiempo. De la misma forma que la música clásica, las tradiciones y la cultura, nos propusimos crear algo **inmortal en el tiempo**. Algo que se traspase entre generaciones sin jamás envejecer, siempre enseñando y descubriendo nuevos caminos hacia un sano y enriquecedor entretenimiento.

Con estos principios sólidos presentes, **Based Games** inició rápidamente el bosquejo de un juego tipo *bullet hell*. De la misma forma en la que un agricultor al sembrar una semilla sabe que llegará un día en el que en ese mismo suelo habrá un enorme y fuerte árbol de raíces firmes y profundas, nosotros sabemos que un día lo que empezó como el bosquejo de un proyecto para la universidad se convertirá en el mayor de los referentes a nivel mundial.

> Un viaje de mil millas comienza con el primer paso.
> - *Lao Tse*