+++
title = "Más allá de los videojuegos: KOI-456"
date = "2022-07-14"
author = "[B]ased Games"
+++

Tras varios meses de arduo trabajo, con el equipo de **[B]ased Games** estamos terminando nuestro primer proyecto. Con mucho orgullo, es de nuestro agrado presentarles *KOI-456*, el juego más épico de todos los tiempos.

{{< youtube PUXvBE9u494 >}}

*KOI-456* ha sido cuidadosamente diseñado para ser una auténtica revolución en la industria de los videojuegos. Actualmente ya corre en sistemas con Windows y GNU/Linux. Sin embargo, nosotros no nos conformamos con simplemente dar un giro completo al mundo con *KOI-456*. Como nuestro primer videojuego, para **[B]ased Games** esto no es más que **el inicio de un nuevo e innovador futuro para gamers y desarrolladores**, un futuro donde ambos se encuentran mucho más cercanos que hasta ahora, un futuro donde **todos desarrollamos y disfrutamos los juegos de video**.
