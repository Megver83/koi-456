+++
title = "KOI-456 ha llegado para quedarse"
date = "2022-07-19"
author = "[B]ased Games"
+++

Ya es oficial: **Based Games** hizo el primer lanzamiento de *KOI-456*, disponible en todas las plataformas, todos los sistemas operativos, todas las arquitecturas de procesador, todas las edades, todos los países, todos los idiomas, etc.

{{<youtube i8BWc-foeHM>}}

El momento más esperado del año al fin llegó y lo hizo con todo. *"KOI-456 ha llegado para quedarse"* fue una de las afirmaciones de **César Muñoz**, *CEO* de Based Games, *"fueron sacrificados meses de trabajo para poder desarrollar un videojuego perfecto, con cero bugs, retador y emocionante"*. Sus declaraciones fueron justificadas por su equipo: *"Aunque trabajamos codo a codo entre todos, tuve que dedicarle mucho más tiempo a este proyecto que al resto de ramos del semestre"*, añadió **Giacomo Baldessari**, el principal programador de *KOI-456*, *"lo dejamos todo en pos de crear lo que jamás antes existió y no fue fácil para ninguno"*. **Tomás Vargas**, jefe del departamento de *marketing* de **Based Games**, se refirió justamente a todo lo que tuvo que dejar para sacar adelante un producto tan revolucionario: *"Uno de los mayores y más difíciles retos que escogí fue hacer el mejor tráiler de todos, tuve que dejar a mi polola, el LOL, el carrete y dedicarle todas mis energías a ese único video"*.

Este hito de la humanidad verdaderamente trasciende todo tipo de barreras, porque la imaginación es nuestro único límite. Para nosotros, **Based Games** es mucho más que una empresa de videojuegos, es un símbolo de unión, de compartir el conocimiento, son las ganas de crear cosas nuevas y de cambiar el mundo. Cuando nació *KOI-456*, descubrimos que lo que hicimos fue crear un videojuego tan perfecto e inmejorable que nos comprometimos a hacerlo llegar a cada persona en este mundo, porque el impacto que *KOI-456* tiene sobre una persona, sea quien sea, es un impacto que le cambiará la vida para siempre.

> Solo se vive dos veces: antes y después de KOI-456
>  - *César Muñoz*