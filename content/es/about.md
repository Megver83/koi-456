+++
title = "Acerca de"
+++

![Based Games](/koi-456/img/Based-Games.jpg)

# Based Games

Based Games es una empresa multinacional dedicada al desarrollo y diseño de videojuegos. Ha trabajado con *Epic Games*, *Activision*, *Treyarch*, *Electronic Arts*, *PlayStation* y muchos otros gigantes de los juegos. Inicialmente, comenzamos con un proyecto universitario para crear el mejor juego de la historia -lo que logramos- y luego de nuestro gran éxito descubrimos nuestra mayor pasión: el desarrollo de juegos.

# KOI-456

El primer y único juego que hemos desarrollado. ¿Por qué no más? Porque es el juego más perfecto del mundo de todos los tiempos. Punto.

Puedes conocer la trama del juego y ver una galería de imágenes en [la sección de exhibición](/koi-456/es/showcase)

# Nuestro equipo

- **César Muñoz**: Project Manager, programador
- **Nicolás Cordero**: Soundtrack, Lore, diseñador de juego
- **Giacomo Baldessari**: Programador de Gameplay
- **Lucas Oliver**: Creador de Sprites y texturas, Lore
- **David Pizarro**: Diseño web
- **Tomás Vargas**: Programador
