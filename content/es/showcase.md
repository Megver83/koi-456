---
title: "Exhibición"
---

# Historia

La civilización del planeta KOI-456.04, un mundo similar a la Tierra, nos invade con el objetivo de conquistar el planeta Tierra. Gran parte de la humanidad ha sido diezmada y un plan militar en conjunto entre todos los países reúne a los mejores ingenieros. Estos logran crear prototipos de naves de guerra basados en ingeniería inversa de las naves enemigas.

Como piloto, te dan la orden de defender la Tierra de los extraterrestres, usando estas nuevas naves, como la última de las esperanzas.

# Galería

A continuación se presenta una galería de fotos de *KOI-456*

{{<figure src="/koi-456/img/Menu.png" caption="Menú principal" alt="Imagen: Menú">}}
{{<figure src="/koi-456/img/Options.png" caption="Opciones del juego" alt="Imagen: Opciones">}}
{{<figure src="/koi-456/img/Choose-level.png" caption="Escoger nivel" alt="Imagen: Escoger nivel">}}
{{<figure src="/koi-456/img/Level-1.png" caption="Gameplay nivel 1" alt="Imagen: Nivel 1">}}
{{<figure src="/koi-456/img/Game-over.png" caption="Mensaje al perder" alt="Imagen: Game over">}}
{{<figure src="/koi-456/img/Level-5.png" caption="Gameplay nivel 5" alt="Imagen: Nivel 5">}}
{{<figure src="/koi-456/img/Pause.png" caption="Menú de pausa" alt="Imagen: Pausa">}}
