---
title: "Showcase"
---

# History

The civilization of planet KOI-456.04, an Earth-like world, invades us with the aim of conquering the Earth. Most humans have been decimated and a military plan among all countries brings together the best engineers. These manage to create warships prototypes based on reverse engineering of enemy ships.

As a pilot, you are given the order to defend the Earth from aliens, using these new ships, as the last hope.

# Gallery

You can see a collection of *KOI-456* photos down below.

{{<figure src="/koi-456/img/Menu.png" caption="Main menu" alt="Image: Menu">}}
{{<figure src="/koi-456/img/Options.png" caption="Game options" alt="Image: Options">}}
{{<figure src="/koi-456/img/Choose-level.png" caption="Choose level" alt="Image: Choose level">}}
{{<figure src="/koi-456/img/Level-1.png" caption="Gameplay level 1" alt="Image: Level 1">}}
{{<figure src="/koi-456/img/Game-over.png" caption="Game over message" alt="Image: Game over">}}
{{<figure src="/koi-456/img/Level-5.png" caption="Gameplay level 5" alt="Image: Level 5">}}
{{<figure src="/koi-456/img/Pause.png" caption="Pause menu" alt="Image: Pause">}}
