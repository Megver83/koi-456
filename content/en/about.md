+++
title = "About"
+++

![Based Games](/koi-456/img/Based-Games.jpg)

# Based Games

Based Games is a multinational company dedicated to gaming development and design. It has worked with *Epic Games*, *Activision*, *Treyarch*, *Electronic Arts*, *PlayStation* and many other gaming-giants. Initially, we started with a university project to create the best game ever -and we did it- and after our huge success we discovered our gratest passion: game development.

# KOI-456

The first and only game we have ever developed. Why no more? Because it is the most perfect game in the world of all times. Period.

# Our team

- **César Muñoz**: Project Manager, programmer
- **Nicolás Cordero**: Soundtrack, Lore, game design
- **Giacomo Baldessari**: Gameplay programmer
- **Lucas Oliver**: Sprites and textures artist, Lore
- **David Pizarro**: Web design
- **Tomás Vargas**: Programmer