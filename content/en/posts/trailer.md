+++
title = "KOI-456 arrived to stay"
date = "2022-07-19"
author = "[B]ased Games"
+++

It's official: **Based Games** made the first release of *KOI-456*, available on all platforms, all operating systems, all processor architectures, all ages, all countries, all languages, etc.

{{<youtube i8BWc-foeHM>}}

The most awaited moment of the year finally arrived and did it with everything. *"KOI-456 arrived to stay"* was one of the statements by **César Muñoz**, *CEO* of Based Games, *"months of work were sacrificed in order to develop a perfect video game, with zero bugs, challenging and exciting"*. His statements were justified by his team: *"Although we all worked side by side, I had to dedicate much more time to this project than to the rest of the classes of the semester"*, added **Giacomo Baldessari**, the main programmer of *KOI-456*, *"we left everything behind to create what never existed before and it wasn't easy for anyone"*. **Tomás Vargas**, head of the *marketing* department at **Based Games**, referred precisely to everything he had to give up to bring such a revolutionary product forward: *"One of the biggest and most difficult challenges I chose was to make the best trailer of all, I had to leave my girlfriend, LOL, parties and dedicate all my energies to that one video"*.

This milestone of humanity truly transcends all kinds of barriers, because imagination is our only limit. For us, **Based Games** is much more than a video game company, it is a symbol of unity, of sharing knowledge, it is the desire to create new things and change the world. When *KOI-456* was born, we discovered that what we did was to create a so perfect and unbeatable video game that we promised to reach it to every person in this world, because the impact that *KOI-456* has on a person, whoever this is, it's an impact that will change your life forever.

> You only live twice: before and after KOI-456
> - *César Muñoz*