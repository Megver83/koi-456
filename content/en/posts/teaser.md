+++
title = "Beyond videogames: KOI-456"
date = "2022-07-14"
author = "[B]ased Games"
+++

After several months of hard work, with the **[B]ased Games** team we are finishing our first project. Very proudly, we are pleased to present *KOI-456*, the most epic game of all time.

{{< youtube PUXvBE9u494 >}}

*KOI-456* has been carefully designed to be a true revolution in the video game industry. It currently runs on Windows and GNU/Linux systems. However, we are not satisfied with simply turning the world upside down with *KOI-456*. As our first video game, for **[B]ased Games** this is nothing more than **the beginning of a new and innovative future for gamers and developers**, a future where both are much closer than before, a future where **everyone develops and enjoys video games**.