+++
title = "Based Games, the new paradigm in entertainment"
date = "2022-03-15"
author = "César Muñoz"
cover = "img/Based-Games.jpg"
+++

Everything began one day in March 2022, at the Catholic University of the North in Coquimbo, Chile. It was the beginning of presential classes. That day, [six outstanding students](/koi-456/about/#our-team) of the computing engineering career were put to the test the greatest and most dizzying challenge: to develop the best videogame never imagined before. A challenge that, with courage, the attractive students did not hesitate to accept.

This is how **Based Games** was born, a team of enthusiasts made up of talented artists and programmers with the aim of creating something more than an impressive video game: to generate an unforgettable experience. Not a fad, not a trend. We dream of doing something that exceeds the limits of time. In the same way as classical music, traditions and culture, we set out to create something **immortal in time**. Something that is passed between generations without ever getting old, always teaching and discovering new paths towards healthy and enriching entertainment.

With these solid principles in mind, **Based Games** quickly began the outline of a *bullet hell* game. In the same way that a farmer who sows a seed knows that one day there will be a huge and strong tree with firm and deep roots in that same soil, we know that one day what began as the sketch of a project for the university will become the largest reference worldwide.

> A journey of a thousand miles begins with the first step.
> - *Lao Tzu*